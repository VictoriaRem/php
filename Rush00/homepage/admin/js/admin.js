function init() {
	$.post(
		"admin/database/core.php",
		{
			"action" : "init"
		},
		showItem
	);
}

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    console.log(results[1]);
    return results[1] || 0;
}


function showItem(data) {
	data = JSON.parse(data);
	console.log(data);
	var out = "";
	for (var key in data) {

		if ($.urlParam('item') == `${data[key].id}`) {
			out +=`<p>Name: ${data[key].Name}</p>`;
			out +=`<p>Price: ${data[key].Price}</p>`;
			out +=`<p>Material: ${data[key].Material}</p>`;
			out +=`<p>Capacity: ${data[key].Capacity}</p>`;
			out +=`<p>Color: ${data[key].Color}</p>`;
			out +=`<p>More Details: ${data[key].More}</p>`;
		}
	}
	$('#wrapper_3').html(out);
}


$(document).ready(function () {
	init();
});

