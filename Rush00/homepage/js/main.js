var list = {};
var cart = {};
var out_cart = 0; 
var out_list = 0; 

function init() {
	$.post(
		"admin/database/core.php",
		{
			"action" : "init"
		},
		itemOut
	);
}

function editItemOpen() {
	$.post(
		"admin/database/core.php",
		{
			"action" : "init"
		},
		addItem	
	);
}

function itemOut(data) {
	var data = JSON.parse(data);
	// console.log(data);
	var out_mug = "";
	var out_keys = "";
	var out_candle = "";
	for (var key in data) {
		if (data[key].Type == 'mugs') {
			out_mug +=	'<div class="items">' + 
					 	`<img class="item" src="./items/${data[key].Img}" data-id="${key}" alt="${data[key].Type}">` + 
						`<button class="like" data-id="${key}"></button>` + 
						`<button class="cart" data-id="${key}"></button>` + 
						`<p id="description">${data[key].Name}.${data[key].Price} UAH</p>` +
						'</div>';
			$(`#${data[key].Type}`).html(out_mug);
		}

		if (data[key].Type == 'key_rings') {
			out_keys +='<div class="items">' + 
					`<img class="item" src="./items/${data[key].Img}" data-id="${key}" alt="${data[key].Type}">` + 
					`<button class="like" data-id="${key}"></button>` + 
					`<button class="cart" data-id="${key}"></button>` + 
					`<p id="description">${data[key].Name}.${data[key].Price} UAH</p>` +
					'</div>';
			$(`#${data[key].Type}`).html(out_keys);	
		}

		if (data[key].Type == 'candlesticks') {
			out_candle +='<div class="items">' + 
					`<img class="item" src="./items/${data[key].Img}" data-id="${key}" alt="${data[key].Type}">` + 
					`<button class="like" data-id="${key}"></button>` + 
					`<button class="cart" data-id="${key}"></button>` + 
					`<p id="description">${data[key].Name}.${data[key].Price} UAH</p>` +
					'</div>';
			$(`#${data[key].Type}`).html(out_candle);	
		}			
	}


	$('#tablinks').on('onmouseover', addItem); 
	$('.like').on('click', addToList);
	$('.cart').on('click', addToCart);
	$('.item').on('click', goToDescription);
}


function addToList () {
	var id = $(this).attr('data-id');
		
	if (list[id]== undefined) {
		list[id]= 1;
	}
	else {
		list[id]++;
	}
	var container = "";
		container +='<span id="list" class="btn_counter_wish">'+
		'</span>';
	$(`#wish_container`).html(container);
	out_list++
	$(`#list`).html(out_list);
	saveToList();
}
	
function addToCart () {

	var id = $(this).attr('data-id');

	if (cart[id]== undefined) {
		cart[id]= 1;
	}
	else {
		cart[id]++
	}
	var container = "";
		container +='<span id="cart" class="btn_counter_cart">'+
		'</span>';
	$(`#cart_container`).html(container);
	out_cart++
	$(`#cart`).html(out_cart);
	saveToCart();
}

function saveToCart () {
	localStorage.setItem('cart', JSON.stringify(cart));
	localStorage.setItem('out_cart', JSON.stringify(out_cart));
}

function saveToList() {
	localStorage.setItem('list', JSON.stringify(list));
	localStorage.setItem('out_list', out_list);
}

function goToDescription() {
	var id = $(this).attr('data-id');
	 window.location="http://localhost/php/Rush00/homepage/f_json.php?item="+id;
}



function addItem (data) {
	var data = JSON.parse(data);
	var option_list = '<select style="width: 100px; outline: none; display: inline-block;">';
	option_list +='<option data-id="0">New Item</option>';	
		for (var id in data) {
				option_list +=`<option data-id="${id}">${data[id].Name}</option>`;		
			}
		option_list += '</select>';
		$('.option').html(option_list);
		$('.option select').on('change', selectOneItem);
}

function selectOneItem () {
	var id = $('.option select option:selected').attr('data-id');
	console.log(id);
	$.post(
		"admin/database/core.php",
		{
			"action" : "selectOneItem",
			"data-id" : id
		},
			function(data) {
				var data = JSON.parse(data);

				$('#iname').val(data.Name);
				$('#iprice').val(data.Price);
				$('#imat').val(data.Material);
				$('#icap').val(data.Capacity);
				$('#iclr').val(data.Color);
				$('#imore').val(data.More);
				$('#i_img').attr("src", `items/${data.Img}`);
				$('#i_id').val(data.id);
				$('#itype').val(data.Type);
			}
	);		
}

$('.AddtoDB').on('click', SavetoDB);

function SavetoDB () {
	var id = $('#i_id').val();
	
	if (id !== '') {

		$.post(
			"admin/database/core.php",
			{
				"action" : "updateItems",
				"i_id" : id,
				"iname" : $('#iname').val(),
				"iprice" : $('#iprice').val(),
				"imat" : $('#imat').val(),
				"icap" : $('#icap').val(),
				"iclr" : $('#iclr').val(),
				"imore" : $('#imore').val(),
				"i_img" : $('#i_img').val(),
				"itype" : $('#itype').val()
			},
			function(data) {
			}
		);
	}
	else {
		$.post(
			"admin/database/core.php",
			{
				"action" : "newItems",
				"i_id" : '',
				"iname" : $('#iname').val(),
				"iprice" : $('#iprice').val(),
				"imat" : $('#imat').val(),
				"icap" : $('#icap').val(),
				"iclr" : $('#iclr').val(),
				"imore" : $('#imore').val(),
				"i_img" : $('#i_img').val(),
				"itype" : $('#itype').val()
			},
			function(data) {
			}
		);	
	}
}


// $('#i_img').attr("src", `items/${data.Img}`);

function loadCart() {

	if (localStorage.getItem('cart')) {
		cart =  JSON.parse(localStorage.getItem('cart'));
		out_cart = localStorage.getItem('out_cart');
		if (out_cart != 0) {

		var container = "";
			container +='<span id="cart" class="btn_counter_cart">'+
			'</span>';
		$(`#cart_container`).html(container);
		$(`#cart`).html(out_cart);
		}
		else {
			var container = "";
			container +='';
		$(`#cart_container`).html(container);
		}
	}
}

function loadList() {

	if (localStorage.getItem('list')) {
		list =  JSON.parse(localStorage.getItem('list'));
		out_list = localStorage.getItem('out_list');
		if (out_list != 0) {
		var container = "";
			container +='<span id="list" class="btn_counter_wish">'+
			'</span>';
		$(`#wish_container`).html(container);
		$(`#list`).html(out_list);	
		}
		else {
			var container = "";
			container +='';
		$(`#wish_container`).html(container);
		}	
	}
}


// function loadCartDetails() {
	
// 	if (localStorage.getItem('cart')) {

// 		var cart =  JSON.parse(localStorage.getItem('cart'));
// 		out_cart = localStorage.getItem('out_cart');
		

// 		for (var key in cart) {
// 			console.log(key);
// 			for (var value in key) {
// 				// console.log(cart[key[value]]);

// 				$.post(
// 					"admin/database/core.php",
// 					{
// 						"action" : "loadCartDetails",
// 						"key" : key
// 					},
// 						function(data) {
							
// 								var data = JSON.parse(data);
// 								var out_item = "";
// 								console.log(data);
// 								// $('#i_id').val(key);
// 								// $('#i_img').attr("src", `items/${data.Img}`);
								
// 								for (var key in data) {
// 									var quant = data[key].Price * cart[key[value]];
// 									out_item +=	'<tr id="cart_item_info">' +
// 										`<td id="i_id" style="width: 25px;">${key}</td>` +
// 										`<td id="i_img" style="width:180px;"><img style="width: 100px; height: 100px;" src="./items/${data[key].Img}"></td>` +
// 										`<td id="idesc" style="width: 100px;">${data[key].Type}</td>` +
// 										`<td id="iprice" style="width: 100px;">${data[key].Price} UAH</td>` +
// 										`<td id="iquant" style="width: 20px;">${cart[key[value]]}</td>` +
// 										`<td id="itotal" style="width: 100px;">${quant} UAH</td>` +
// 										'</tr>';
// 										$(`#info`).html(out_item);
// 									}
									
// 									// console.log(out_item);


							
// 						}
// 				);
// 			}	
// 		}
// 	}
// 	else {
// 		$('#cart_details_container').html('The cart is empty'); 
// 	}
// }


$(document).ready(function () {
	init();
	editItemOpen();
	loadCart();
	loadList();
	// loadCartDetails();
});


