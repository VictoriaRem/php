<?php
$file = "./private/passwd";

if ($_REQUEST === "" || $_POST["submit"] !== "OK") { 
echo "ERROR\n";
return;	
}
print_r($_REQUEST);

$flag = 0;
$modif_user = unserialize(file_get_contents($file));

foreach ($modif_user as $key => $valid_user) {
		if ($valid_user["login"] === $_POST["login"]) {
			if ($valid_user["passwd"] === hash("whirlpool", $_POST["oldpw"])) {
				$modif_user[$key]['passwd'] = hash("whirlpool", $_POST['newpw']);
				$flag++;
				header("Location:http://localhost/php/d4/04/index.html");
				break;	
			}
		}	
}

if ($flag == 0) {
	echo "ERROR\n";
	return;	
}

file_put_contents($file, serialize($modif_user));
echo "OK\n";
return;

 // curl -d login=vika -d oldpw=vish -d newpw=vish -d submit=OK "http://localhost/php/d4/04/modif.php"
?>
