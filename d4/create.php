<?php

$path = './private';
$file = './private/passwd';

if ($_POST["login"] === "" || $_POST["passwd"] === "" || $_POST["submit"] !== "OK") { 
	echo "ERROR\n";
	return;
}
if (!file_exists($path)) {
	mkdir($path, 0755, true);
}
	
if (file_exists($file)) {	

$new_user = unserialize(file_get_contents($file));
	foreach ($new_user as $valid_user) {
		if ($valid_user["login"] === $_POST["login"]) {
		echo "ERROR\n";
		return;
		}		
	}				
}

$data["login"] = $_POST["login"];
$data["passwd"] =  hash("whirlpool", $_POST["passwd"]);
$new_user[] = $data;
file_put_contents($file, serialize($new_user));
echo "OK\n";						

?>
