<?php

$kaka = fopen("php://stdin", "r");

while (!feof($kaka)) {
	echo "Enter a number: "; 
	$num = trim(fgets($kaka));
	
	if ($num || $num == 0) {
		if (is_numeric($num)) {
		 
			if ($num % 2 == 0) 
		 		echo "The number $num is even\n"; 
		 	else 
		 		echo "The number $num is odd\n";
		}
		else
		 	echo "'$num' is not a number\n";
	}
}

fclose($kaka);
?>